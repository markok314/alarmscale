# AlarmScale

Alarm Scale is intended for people, who have to eat or drink food or medicine 
at specific time of the day or night. If the glass is not taken of the scale
until specific time, and returned empty (lighter) few minutes after that, 
sound alarm is triggered.

The scale has resolution of 1g, so it is not intended for small weights 
like tablets. Difference between full and empty glass should be at 
least 10 grams.
