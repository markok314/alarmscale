This document describes how CS scale with alarm was implemented.


Installation to Raspberry Pi
============================

1. When installing raspbian lite, enter hostname `tehtnica`.

$ cd /usr/bin
$ sudo ln -fs python3.7 python
$ cd
$ sudo apt install python3-pip

1. Install C/C++ gpio lib to be used by C++ read utility:

   $ sudo apt install libgpiod-dev

2. To be able to run python script, you have to install the following:

        pip3 install pygame  # if _pygame_ sound is used (currently used by the script)
        pip3 install RPi.GPIO
        pip3 install PyYAML    # required for parsing of command from mobile app
        sudo apt install libsdl-mixer1.2 git
        sudo timedatectl set-timezone Europe/Ljubljana
        # ** Program MUST be checked out in** `/home/pi` so that sound file is located
        # in dir `/home/pi/alarmscale/resources/submarine...`:
        cd
        git clone https://gitlab.com/markok314/alarmscale.git

   To able to run bluetoothServer.py as normal user (without sudo), this links
   are helpful:
   https://stackoverflow.com/questions/34599703/rfcomm-bluetooth-permission-denied-error-raspberry-pi
   https://github.com/ev3dev/ev3dev/issues/274#issuecomment-74593671

   In short:
   - add user pi to gorup bluetooth:
   sudo adduser pi bluetooth  # check with: cat /etc/group | grep bluetooth
   - change group of /var/run/sdp, which is created on boot, so you have to
     change it after every reboot. Links above describe one way, but I added

     sudo chgrp bluetooth /var/run/sdp

     to bluetoothServer.py

For sound controller pygame is recommended, but you have to run

    alsamixer

and set <Headphone> volume to 100% (up to top, including red area). It seems that
`omxplayer` does not use alsamixer, so its volume is already on 100%.

To get sound, set also `dtoverlay` as described below.

If _omxplayer_ will be used instead of pygame sound controller:

    sudo apt install omxplayer

Bluetooth
---------
   sudo apt-get install bluetooth bluez blueman libbluetooth-dev
   sudo python3 -m pip install pybluez

   1. Edit /etc/systemd/system/dbus-org.bluez.service
      and add '-C' after 'bluetoothd'. Reboot. (-C means compatibility
      mode)
      $ sudo vim /etc/systemd/system/dbus-org.bluez.service

   2. $ sudo sdptool add SP  # this adds serial protocol only until
                             # restart. For permanent activation:
      $ sudo vim /etc/systemd/system/dbus-org.bluez.service
        # add line after 'ExecStart=', which you modified above:
        ExecStartPost=/usr/bin/sdptool add SP

   sudo reboot

   sudo hciconfig hci0 piscan  # without this python server reports
                               # '_bluetooth.error: no advertisable device'

Pairing:

    sudo bluetoothctl
    [bluetooth]# scan on   # type this if you do not know device ID.
                           # Then wait, until nearby devices are found.
    [bluetooth]# pair xx:xx:xx:xx:xx:xx:xx:xx  # confirm pairing on
                                               # phone and raspi
    [bluetooth]# trust xx:xx:xx:xx:xx:xx:xx:xx
    [bluetooth]# discoverable on   # Make raspi discoverable. You
                                   # actually have to do this after
                                   # every restart.

   To pair tehtnica and raspi, I clicked 'tehtnica' on phone and
   then 'Yes' on phone. Done. No need to do anything else on RPi.

See also: https://github.com/ole-vi/bluetooth-server


Check/set timezone
------------------

    timedatectl status
    sudo timedatectl set-timezone Europe/Ljubljana


For development
---------------

    sudo apt install neovim tmux silversearcher-ag


Start on boot
-------------

There are several ways, from modifying /etc/rc.local, ..., but the best I
found is:

    crontab -e

Add the following line:

    @reboot python /home/pi/alarmscale/src/alarmScaleMain.py > /home/pi/alarmscale.log 2>&1
    @reboot python /home/pi/alarmscale/src/bluetoothServer.py > /home/pi/bluetoothServer.log 2>&1


Scale
=====

   I bought Load Cell with green HX711 AD converter, mounted it (see image
   in this dir) and connected to RPi according to
   (https://tutorials-raspberrypi.com/digital-raspberry-pi-scale-weight-sensor-hx711/)

   Excerpt from the page above (just in case if the page above disappears):

   The four cables of the Load Cell must be connected to HX711. It has
   six connections, of which we only need four. The connection is as
   follows:

   > Red: E+
   > Black: E-
   > Green: A-
   > White: A+

   The pins labeled `B+/B-` remain empty. Apparently there are versions
   of the sensor, where the pins are labeled `S+/S-` instead of `A+/A-`.

   Now you just have to connect the sensor to the Raspberry Pi. Since
   this also has only four connections, the wiring is quite simple:

   > VCC to Raspberry Pi Pin 2 (5V)
   > GND to Raspberry Pi Pin 6 (GND)
   > DT to Raspberry Pi Pin 29 (GPIO 5)
   > SCK to Raspberry Pi Pin 31 (GPIO 6)

   See ![Connection diagram](Raspberry-Pi-HX711-Steckplatine-600x342.png "Connection diagram")


   Software
   --------

   I used and modified software from

       git clone https://github.com/tatobari/hx711py

   See dir `../src` for my adapted version.

   See testProcedure.md to see how to run the program.


   To start at boot ad the following line:

       python /home/pi/alarmscale/src/alarmScaleMain.py > /home/pi/alarmscale.log 2>&1 &

   to /etc/rc.local, before 'exit 0'

   See ~/alarmscale.log for logged errors.

   Because it turned out that Python is too slow to always keep SCK pulse below
   50 us, I implemented C++ version `src/hx711-cpp/hx711.cpp`. To build it:

       cd ~/alarmscale/src/hx711-cpp
       g++ hx711.cpp -lgpiod -o hx711


   Noise
   -----
   There is some random noise present in scale measurements. I'm not
   sure if it is analog noise or error in SPI communication, although
   there are some indications that is the digital part - some bits get
   toggled. I also tried to wrap hx711 board into ALU foil without
   success. Nois did not change when I change the sensor (load cell
   and electronics, although they are from the same manufactureer).
   Also, noise increases after the speaker has been turned on (but
   then turned off again - the increaded noise remains).
   Some data measured with alarmscale/src/hx711/example.py:

   # Avoid interference from alarmscale and bluetooth running in
   # the background:
   pi@tehtnica:~ $ pkill python
   pi@tehtnica:~ $ pkill python

   Before speaker was turned on, 15 wrong measurements in 25 minutes:
       Sun Aug 21 20:54:00 CEST 2022
       pi@tehtnica:~/alarmscale/src/hx711 $ python example.py
       counter = 1 7346.256921832959 141
       counter = 2 -116.51908656516952 346
       counter = 3 3614.868917633895 431
       counter = 4 -116.51908656516952 491
       counter = 5 116.69266369727201 737
       counter = 6 816.3279144845965 1424
       counter = 7 116.69266369727201 1536
       counter = 8 14.66252295745384 1544
       counter = 9 -116.51908656516952 2254
       counter = 10 816.3279144845965 2357
       counter = 11 7346.256921832959 2499
       counter = 12 816.3279144845965 2564
       counter = 13 7346.256921832959 2614
       counter = 14 7346.256921832959 2864
       counter = 15 116.69266369727201 3154
       Sun Aug 21 21:19:35 CEST 2022


   After speaker was turned on and no reboot (16 in 9 min):
       Sun Aug 21 21:27:30 CEST 2022
       pi@tehtnica:~/alarmscale/src/hx711 $ python example.py
       counter = 1 815.9057345693291 51
       counter = 2 116.27048378200452 185
       counter = 3 349.48223404444605 252
       counter = 4 7345.834741917692 278
       counter = 5 349.48223404444605 345
       counter = 6 116.27048378200452 418
       counter = 7 28.816077433588955 421
       counter = 8 1748.7527356190951 499
       counter = 9 28.816077433588955 554
       counter = 10 815.9057345693291 575
       counter = 11 14.24034304218636 609
       counter = 12 116.27048378200452 798
       counter = 13 -116.941266480437 868
       counter = 14 57.96754621639415 871
       counter = 15 349.48223404444605 1052
       counter = 16 3614.4467377186274 1091
       Sun Aug 21 21:36:10 CEST 2022


    After hx711 boared was wrapped into ALU foil (20 in 14 min):
       Sun Aug 21 21:45:16 CEST 2022
       pi@tehtnica:~/alarmscale/src/hx711 $ python example.py
       counter = 1 28.575085948357852 111
       counter = 2 -117.1822579656681 150
       counter = 3 7345.59375043246 165
       counter = 4 116.02949229677343 322
       counter = 5 -117.1822579656681 442
       counter = 6 -117.1822579656681 494
       counter = 7 116.02949229677343 643
       counter = 8 57.72655473116304 759
       counter = 9 1748.5117441338639 762
       counter = 10 28.575085948357852 986
       counter = 11 13.999351556955256 1034
       counter = 12 7345.59375043246 1244
       counter = 13 -117.1822579656681 1474
       counter = 14 1748.5117441338639 1509
       counter = 15 7345.59375043246 1512
       counter = 16 3614.205746233396 1568
       counter = 17 -117.1822579656681 1571
       counter = 18 349.2412425592149 1590
       counter = 19 57.72655473116304 1894
       counter = 20 116.02949229677343 2028
       Sun Aug 21 21:59:56 CEST 2022

counter = 1 7345.229890852011 70 8388607 0b11111111111111111111111
counter = 2 348.87738297876547 143 524287 0b1111111111111111111
counter = 3 1748.1478845534145 146 2097151 0b111111111111111111111
counter = 4 348.87738297876547 369 524287 0b1111111111111111111
counter = 5 57.3626951507136 461 196607 0b101111111111111111
counter = 6 348.87738297876547 464 524287 0b1111111111111111111
counter = 7 815.3008835036485 520 1048575 0b11111111111111111111
counter = 8 57.3626951507136 539 196607 0b101111111111111111
counter = 9 115.66563271632398 582 262143 0b111111111111111111
counter = 10 115.66563271632398 638 262143 0b111111111111111111
counter = 11 57.3626951507136 649 196607 0b101111111111111111
counter = 12 7345.229890852011 665 8388607 0b11111111111111111111111
counter = 13 28.21122636790841 697 163839 0b100111111111111111
counter = 14 57.3626951507136 903 196607 0b101111111111111111
counter = 15 1748.1478845534145 988 2097151 0b111111111111111111111
counter = 16 3613.841886652947 1149 4194303 0b1111111111111111111111
counter = 17 57.3626951507136 1315 196607 0b101111111111111111
counter = 18 348.87738297876547 1318 524287 0b1111111111111111111
counter = 19 815.3008835036485 1321 1048575 0b11111111111111111111
counter = 20 348.87738297876547 1578 524287 0b1111111111111111111
counter = 21 115.66563271632398 1610 262143 0b111111111111111111
counter = 22 348.87738297876547 1885 524287 0b1111111111111111111
counter = 23 -117.54611754611754 1901 -1 -0b1
counter = 24 348.87738297876547 1972 524287 0b1111111111111111111
counter = 25 57.3626951507136 2030 196607 0b101111111111111111
counter = 26 57.3626951507136 2109 196607 0b101111111111111111
counter = 27 28.21122636790841 2167 163839 0b100111111111111111
counter = 28 815.3008835036485 2238 1048575 0b11111111111111111111
counter = 29 3613.841886652947 2435 4194303 0b1111111111111111111111
counter = 30 7345.229890852011 2558 8388607 0b11111111111111111111111
counter = 31 7345.229890852011 2561 8388607 0b11111111111111111111111
counter = 32 7345.229890852011 2564 8388607 0b11111111111111111111111
counter = 33 1748.1478845534145 2669 2097151 0b111111111111111111111
counter = 34 3613.841886652947 2983 4194303 0b1111111111111111111111
counter = 35 7345.229890852011 3033 8388607 0b11111111111111111111111
counter = 36 115.66563271632398 3216 262143 0b111111111111111111
counter = 37 815.3008835036485 3274 1048575 0b11111111111111111111
counter = 38 -117.54611754611754 3363 -1 -0b1
counter = 39 815.3008835036485 3497 1048575 0b11111111111111111111





   Calibration
   ------------

   Run:

       sudo python alarmscale/src/alarmScaleMain.py

   Put weight of 500 g (half the range of the Load cell) on scale,
   and record `value - tare`, for example 487050. Then modify the code:

       hx.set_reference_unit(487050 / 500)

   Test the setup.


   Changing weight sensor HX711
   ---------------------------

   It shouldn't but it happened that the sensor stopped working without any 
   visible damage.
   In that case replace the sensor, but then **you have to ealibrate it!**


Alarm
=====

See speaker-amplifier-circuit.pdf in this dir for audio amplifier.

IMPORTANT: Inputs are not 5V tolerant!
           http://www.mosaic-industries.com/embedded-systems/microcontroller-projects/raspberry-pi/gpio-pin-electrical-specifications

Because amplifier has resistor from transistor colector to base, there is
constant current of almost 100 mA. Additionally, you can hear RPi noise
on speaker. To fix these problems, I added 2N2222 NPN transistor controlled
by additional RPi output. It connects amplifier transistor base to ground,
thus effectively turning amplifier off. Only about 2 mA current remains through
2k2 resistor and 2N2222 to ground.



RPi connections
===============

RPi header pin numbers |  function | wire color
-----------------------------------
2 | scale Vcc (ADC HX711) | red
3 | Red LED               | brown
4 | speaker amplifier Vcc | red
5 | Green LED             | black
6 | scale GND             | black
7 | Yellow LED            | red
11 | Input Switch (key)   | white
12 | Output connected to speaker amplifier input (+ of capacitor) | pink
14 | GND                  | brown
29 | scale DT             | white
31 | scale SCK            | brown
40 | speaker amplifier on/off (base of 2N2222 | violet

To redirect audio from jack to `Pin 12`, I had to add:

    dtoverlay=pwm-2chan,pin=18,func=2,pin2=13,func2=4

to `/boot/config.txt`.

Comment from
[https://www.raspberrypi.org/blog/tinkernut-diy-pi-zero-audio]:

> The above dtoverlay= line expects the use of `GPIO 18` and `GPIO 13`
> for the GPIO's used for PWM audio, but GPIO 19 is also a
> possibility.

I connected only `Pin 12`, because mono is OK for alarm.

To play sound from command line:

    omxplayer submarine-diving-alarm.mp3

Note: Once Pin 12 is configured as In, or Out, or PWM in Python, omxplayer
produces no sound. I had to reboot rpi.


Software
========

See dir ../src for Python sources.

There are two scripts:
alarmScaleMain.py - reads scale, controlls LEDs and speaker
bluetoothServer.py - waits for connection on bluetooth from mobile app.
Communicates with alarmScaleMain via files, currently /tmp/settings.out
and /tmp/settings.in. The 'in' file contains starch mode (Corn Starch or
Glycosade), and is written by bluetoothServer when received from mobile app,
and then read by alarmScaleMain and deleted by alarmScaleMain. Every
time alarmScaleMain reads the 'in' file, and on startup, it writes current
settings to the 'out file'. After bluetoothServer receives the info from
mobile app, and writes it to the 'in' file, it waits until modification
time of the 'out' file changes, and then reads the 'out' file, appends current
time, and sends it in response to the mobile app. It then updates information
on the screen.


Login to installed system
=========================

This might be useful to check log files, ...
If you want to update configuration only (for example times of alarms, see
alarmScaleMain.py, search for string AlarmTime), then it is highly
recommended to modify file in working copy, commit it, and then scp it to RPi.
Only this way you can restore alarmscale to the last working configuration
in case of SD card failure or similar problem.

ssh pi@tehtnica.local
scp <filename> pi@tehtnica.local:<remote directory>

For example:
scp src/alarmScaleMain.py pi@tehtnica.local:alarmscale/src

