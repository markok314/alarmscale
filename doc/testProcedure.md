This document desribes test procedure for alarm scale. To execute it:
- ssh to rpi
- edit alarmscale/src/alarmScaleMain.py and set IS_DEBUG = True, IS_TEST = True
- cd ~/alarmscale
- python src/alarmScaleMain.py


1. Regular procedure without alarm. Instructions are printed on screen during 
   program run.

2. Do not put glass with CS on scale. There must be alarm at
   CS_MUST_BE_ON_SCALE_START played once.
   'ERROR: There is no glass with CS on scale at expected time: ...' must be logged.
   a) Put glass with CS on. Execution should proceeed normally.
   b) Wait for whole day and check that alarm is again triggered on the next day.

3. Put glass on scale BEFORE CALIBRATION. There must be alarm 
   when time >= CALIBRATION_TIME. Alarm must be played only once,
   'ERROR: Something is already on scale!' and 
   'ERROR: Calibration error' msg must be logged. This state is left only
   when everything is taken off scale.

4. Put glass on scale after calibration and before START_TIME. Take glass off
   scale:
   - before midnight
   - after midnight, but before CS_MUST_BE_ON_SCALE_END.
   Alarm must play until glass is put back on scale. 
   'ERROR: Somebody moved CS off scale too early!' must be logged.
   a) Put glass back. Execution sgould procedd normally
   b) Wait for whole day and check that execution proceesds normally

5. Put glass on scale, do not take it off on time. Alarm must play until
   glass is taken off.
   'ERROR: CS was NOT taken off scale!' must be logged.
   a) Do not put empty glass back. Alarm must play until empty glass is put back.
      'ERROR: Glass has not been put back or not all CS was consumed: ...' must be logged. 
   b) Put empty glass back and press switch. No alarm should be played.

6. Put glass on scale, wait until CS_MUST_BE_ON_SCALE_END. Take glass off,
   Do not put empty glass back. Alarm must play until empty glass is put back.
   'ERROR: Glass has not been put back or not all CS was consumed: ...' must be logged. 

7. Do not take glass off scale on time, to trigger long alarm. Wait for MAX_ALARM_DURATION_S,
   then alarm should shut down. Press switch to resume program. Perform one normal cycle.

Tests performed
Date
2020-01-18 Marko
2020-01-26 MArko
