LED legenda
===========

__oranžna utripa__ - dan, tehtnica mora biti prazna

__zelena utripa__ - čas za odložitev kozarca s škrobom na tehtnico

__oranžna sveti__ - kozarec s škrobom zaznan, če ga odstraniš bo alarm

__zelena sveti__ - lahko vzameš kozarec s škrobom s tehtnice. Časovno okno je pribl.
pol ure, zato trenutek, ko se prižge zelena dioda, ni nujno že čas za uporabo škroba

__oranžna in rumena utripata__ - vrni prazen kozarec na tehtnico

__oranžna in rumena svetita__ - kone cikla, stanje na tehtnici ni pomembno


Po vklopu je tehtnica vedno nastavljena na koruzni �krob.
