#!/usr/bin/python3

# This module handles NTP time synchronization on RPi.
#
# Example for setting time on rpi, and turning network time syc on/off:
#   $ sudo timedatectl set-ntp false
#   $ sudo timedatectl set-time '2017-10-02 21:59'
#   $ date
#   Mon Oct  2 21:59:06 CEST 2017
#   $ sudo timedatectl set-ntp true
#   $ date
#   Thu Sep 10 21:23:02 CEST 2020
#
#   $ sudo timedatectl show
#   Timezone=Europe/Ljubljana
#   LocalRTC=no
#   CanNTP=yes
#   NTP=yes
#   NTPSynchronized=yes
#   TimeUSec=Thu 2020-09-10 21:32:47 CEST

import subprocess as sp
import datetime as dtm

KEY_NTP_SYNCHRONIZED = 'NTPSynchronized'
KEY_NTP = 'NTP'
VALUE_YES = 'yes'

def getNtpProperty(propertyName: str) -> str:
    ntpStatus = sp.check_output(['sudo', 'timedatectl', 'show']).decode('utf8')
    # 'Timezone=Europe/Ljubljana\nLocalRTC=no\nCanNTP=yes\nNTP=yes\nNTPSynchronized=yes\nTimeUSec=Thu 2020-09-10 21:45:58 CEST\n'
    key_value = ntpStatus.split('\n')
    # ['Timezone=Europe/Ljubljana', 'LocalRTC=no', 'CanNTP=yes', 'NTP=yes', 'NTPSynchronized=yes', 'TimeUSec=Thu 2020-09-10 21:45:58 CEST', '']
    for q in key_value:
        if '=' in q:   # the last line is empty
            k, v = q.split('=')
            if k == propertyName:
                return v

    return None


def isTimeSyncActive() -> bool:
    return getNtpProperty(KEY_NTP_SYNCHRONIZED) == VALUE_YES


def isNtpActive() -> bool:
    return getNtpProperty(KEY_NTP) == VALUE_YES


def setNtpActivity(state: bool):

    newState = 'true' if state else 'false'
    sp.check_call(['sudo', 'timedatectl', 'set-ntp', newState])


def setTimeManually(dateAndTime: str):
    """
    Example command:
      $ sudo timedatectl set-time '2017-10-02 21:59'
    Returns string with system time - should equal input time.
    """
    # turno NTP off, otherwise time setting is not possible
    if isNtpActive():
        setNtpActivity(False)

    sp.check_call([f"sudo timedatectl set-time '{dateAndTime}'"], shell=True)


def currentTime() -> str:
    n = dtm.datetime.now()
    return f"{n.year}-{n.month:02}-{n.day:02} {n.hour:02}:{n.minute:02}:{n.second:02}"


def main():
    print(setTimeManually('2017-10-02 21:59'))


if __name__ == '__main__':
    main()
