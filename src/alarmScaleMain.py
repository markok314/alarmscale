#!/usr/bin/python

# This module is the top level module for alarm scale. It measures weight
# on scale, and triggers alarm if necessary.
#
# To start at boot add the following line:
#  python /home/pi/alarmscale/src/alarmScaleMain.py > /home/pi/alarmscale.log 2>&1 &
# to /etc/rc.local, before 'exit 0'

import os
import sys
import time
from enum import Enum
import argparse
import shutil
import datetime as dtm
import subprocess as sp
import RPi.GPIO as GPIO
from pygame import mixer
from hx711.hx711 import HX711
import alarm_consts as ac
import pathlib as ptl

# header pin numbers
SWITCH_PIN = 11
SPEAKER_PIN = 12
LED_RED_PIN = 3
LED_GREEN_PIN = 5
LED_YELLOW_PIN = 7
SCALE_DT_PIN = 29
SCALE_SCK_PIN = 31
SOUND_AMPLIFIER_ON_OFF_PIN = 40 # used to pul base of sound amplifier
# transistor to low, to decrease power consumption in idle state 
# almost 100 mA for speaker when silent) and avoid noise on speaker.

IS_DEBUG = True  # False
IS_TEST_MODE = False  # DO NOT MODIFY, use cmd line arg -t to set this to true

CS_CS_WEIGHT_g = 50  # CS weight when CS is administered at 23:00
GLYCOSADE_CS_WEIGHT_g = 30  # CS weight when Glycosade is administered at 23:00
CS_WEIGHT_g = CS_CS_WEIGHT_g

WEIGHT_EPSILON_g = 15  # allowed offset, if some wet CS is left in the glass
MAX_WEIGHT_ON_EMPTY_SCALE_g = 50
SCALE_REFERENCE_UNIT = 565200 / 500  # value at 500g weight


MAX_ALARM_DURATION_S = 3600  # 1 hour

SOUND_FILE = "/home/pi/alarmscale/resources/submarine-diving-alarm-daniel_simon.mp3"


class ST(Enum):
    BEFORE_CALIBRATION = 1
    CALIBRATED = 2
    GLASS_WITH_CS_ON = 3
    GLASS_WITH_CS_MAY_BE_TAKEN_OFF = 4
    GLASS_WITH_CS_OFF = 5
    GLASS_EMPTY_ON = 6
    GLASS_NOT_ON_SCALE_ERROR = 7


class AlarmTime:
    def __init__(self, hour: int, minute: int):
        self.hour = hour
        self.minute = minute

    def __lt__(self, other):
        return self.hour < other.hour  or (self.hour == other.hour  and
                self.minute < other.minute)

    def __le__(self, other):
        return self.hour < other.hour  or (self.hour == other.hour  and
                self.minute <= other.minute)

    def __eq__(self, other):
        return self.hour == other.hour and self.minute == other.minute

    def __ne__(self, other):
        return not self.__eq__(other)

    def __gt__(self, other):
        return self.hour > other.hour  or (self.hour == other.hour  and
                self.minute > other.minute)

    def __ge__(self, other):
        return self.hour > other.hour  or (self.hour == other.hour  and
                self.minute >= other.minute)

    def __str__(self):
        return f'{self.hour: 2d}:{self.minute:02d}'

    def next(self):
        """ Returns next minute time. """
        if self.minute == 59:
            if self.hour == 23:
                return AlarmTime(0, 0)
            return AlarmTime(self.hour + 1, 0)

        return AlarmTime(self.hour, self.minute + 1)

    def prev(self):
        """ Returns previous minute time. """
        if self.minute == 0:
            if self.hour == 0:
                return AlarmTime(23, 59)
            return AlarmTime(self.hour - 1, 59)

        return AlarmTime(self.hour, self.minute - 1)

    def test():
        """ Tests this class: import alarmScaleMain as asm; asm.AlarmTime.test() """
        if AlarmTime(12, 10) != AlarmTime(12, 10):
            raise Exception("Test failed!")

        if AlarmTime(12, 10).prev() != AlarmTime(12, 9):
            raise Exception("Test failed!")
        if AlarmTime(12, 10).next() != AlarmTime(12, 11):
            raise Exception("Test failed!")

        if AlarmTime(12, 0).prev() != AlarmTime(11, 59):
            raise Exception("Test failed!")
        if AlarmTime(12, 59).next() != AlarmTime(13, 0):
            raise Exception("Test failed!")

        if AlarmTime(0, 0).prev() != AlarmTime(23, 59):
            raise Exception("Test failed!")
        if AlarmTime(23, 59).next() != AlarmTime(0, 0):
            raise Exception("Test failed!")

        print("OK")


g_starchMode = ac.STARCH_MODE_CORNSTARCH

# Definition of state transition times
# Note: Logic in this module assumes that glass with CS must be put on scale before
# midnight, and remain on until after midnight. Changing state transition times
# across midnight boundary requires also changes of time comparison if statements.

START_OF_DAY_TIME = AlarmTime(7, 10) # LEDs go off at this time  
CALIBRATION_TIME = AlarmTime(12, 0)  # at that hour scale makes calibration 
                                     # and then waits for cornstarch
CS_MUST_BE_ON_SCALE_START = AlarmTime(23, 35) # the latest time when CS must be
                                        # on the scale


def setTimesForStarch(starchMode: str):

    global CS_MUST_BE_ON_SCALE_END, EMPTY_GLASS_IS_BACK_ON_SCALE_TIME, g_starchMode, CS_WEIGHT_g

    log(rf"\/\/\/ setTimesForStarch: {starchMode} \/\/\/")

    if starchMode == ac.STARCH_MODE_CORNSTARCH:
        CS_WEIGHT_g = CS_CS_WEIGHT_g
        CS_MUST_BE_ON_SCALE_END = AlarmTime(3, 5)  # after this time glass with CS
                                                # may be taken off
        EMPTY_GLASS_IS_BACK_ON_SCALE_TIME = AlarmTime(3, 40) # at this time empty glass must
                                                       # be put back on scale
        g_starchMode = ac.STARCH_MODE_CORNSTARCH
        CS_WEIGHT_g = CS_CS_WEIGHT_g

        writeTimesToFile()
    else:

        CS_MUST_BE_ON_SCALE_END = AlarmTime(3, 40)  # after this time glass with CS
                                                # may be taken off
        EMPTY_GLASS_IS_BACK_ON_SCALE_TIME = AlarmTime(4, 40) # at this time empty glass must
                                                       # be put back on scale
        g_starchMode = ac.STARCH_MODE_GLYCOSADE
        CS_WEIGHT_g = GLYCOSADE_CS_WEIGHT_g

        writeTimesToFile()


# Currently only starh mode is preserved between runs (the only value settable
# from phone).
def writeConfigToFile():
    fName = os.path.join(ptl.Path.home(), ac.CONFIG_FILE)
    with open(fName, 'w') as outf:
        outf.write(g_starchMode)
        outf.write('\n')



def readConfigFromFile() -> str:   # may return tuple in the future
    fName = os.path.join(ptl.Path.home(), ac.CONFIG_FILE)
    starchMode = ''
    if os.path.exists(fName):
        with open(fName, 'r') as inf:
            starchMode = inf.readline().strip()

    if starchMode != ac.STARCH_MODE_CORNSTARCH and starchMode != ac.STARCH_MODE_GLYCOSADE:
        starchMode = ac.STARCH_MODE_CORNSTARCH 

    log(f"Configured starch mode = {starchMode}")
    return starchMode


def writeTimesToFile():
    with open(ac.SETTINGS_OUT_FILE, 'w') as of:
        of.write(f'STARCH_MODE: {g_starchMode}\n')
        of.write(f'CALIBRATION_TIME: {CALIBRATION_TIME}\n')
        of.write(f'CS_MUST_BE_ON_SCALE_START: {CS_MUST_BE_ON_SCALE_START}\n')
        of.write(f'CS_MUST_BE_ON_SCALE_END: {CS_MUST_BE_ON_SCALE_END}\n')
        of.write(f'EMPTY_GLASS_IS_BACK_ON_SCALE_TIME: {EMPTY_GLASS_IS_BACK_ON_SCALE_TIME}\n')
        of.write(f'CS_WEIGHT_g: {CS_WEIGHT_g}\n')


class PygameSoundController:

    def __init__(self):
        mixer.init()
        mixer.music.load(SOUND_FILE)
        mixer.music.set_volume(1)  # run also alsamixer from cmd line and set 
                                   # volume also there to get 100% volume.

    def is_alarm_playing(self):
        return mixer.music.get_busy() == 1

    def play_alarm(self):
        if not self.is_alarm_playing():
            GPIO.output(SOUND_AMPLIFIER_ON_OFF_PIN, 0)
            mixer.music.play()

    def stop_alarm(self):
        GPIO.output(SOUND_AMPLIFIER_ON_OFF_PIN, 1)
        mixer.music.stop()


class OmxplayerController:

    def is_alarm_playing(self):
        st = sp.run(["pgrep", "omxplayer"], capture_output=True)
        # log("pgrep stdout = " + str(st.stdout))
        if st.stdout:  # non-empty status means the player is running
            return True
        return False


    def play_alarm(self):
        # omxplayer executable starts omxplayer.bin, which is
        # the reason why we can't use normal process control
        # for omxplayer - it is other process which plays
        # sound, not the one started from python. So we have
        # to resort to pgrep/pkill utilities here.

        GPIO.output(SOUND_AMPLIFIER_ON_OFF_PIN, 0)
        if self.is_alarm_playing():
            log("omxplayer is already running!")
            return 0

        sp.Popen(["omxplayer", "--no-keys",  # --no-keys allows running in background
                  SOUND_FILE], 
                  stdin=sp.DEVNULL, stdout=sp.DEVNULL, stderr=sp.DEVNULL)
        log("Started player!")
        return 1


    def stop_alarm(self):

        GPIO.output(SOUND_AMPLIFIER_ON_OFF_PIN, 1)
        sp.run(["pkill", "omxplayer.bin"])
        log("Terminated player!")


class PwmController:
    """
    This class is an alternative for OmxplayerController
    above. It is much simpler, but does not offer
    possibility of playing mp3 files. Use it, if there
    will be problems with omxplayer.
    """

    FRQ = 200

    def __init__(self):
        GPIO.setup(SPEAKER_PIN, GPIO.OUT)
        self.pwm = GPIO.PWM(SPEAKER_PIN, PwmController.FRQ)

    def play_alarm(self):
        # set frequency again, otherwise it defaults to 1000 Hz
        # after each stop() (bug in RPi library).
        self.pwm.ChangeFrequency(PwmController.FRQ) 
        self.pwm.start(50)

    def stop_alarm(self):
        self.pwm.stop()


def getTimestamp():
    n = dtm.datetime.now()
    return f"{n.year}-{n.month}-{n.day} {n.hour}:{n.minute}:{n.second} {n.microsecond}"


g_logFile = None
# always log to defult user pi's home dir, also when run by crontab
LOG_FILE_NAME = '/home/pi/alarmScale.log'
OLD_LOG_FILE_NAME = '/home/pi/alarmScale.log1'
OLDEST_LOG_FILE_NAME = '/home/pi/alarmScale.log2'

def openLog():
    global g_logFile

    # rotate log files
    if (os.path.exists(OLD_LOG_FILE_NAME)):
        shutil.copyfile(OLD_LOG_FILE_NAME, OLDEST_LOG_FILE_NAME)
    if (os.path.exists(LOG_FILE_NAME)):
        shutil.copyfile(LOG_FILE_NAME, OLD_LOG_FILE_NAME)

    g_logFile = open(LOG_FILE_NAME, 'w')


def logi(msg: str):
    print(getTimestamp() + ':', msg)
    sys.stdout.flush()
    sys.stderr.flush()
    print(getTimestamp() + ':', msg, file=g_logFile)
    g_logFile.flush()



def log(msg: str):
    if IS_DEBUG:
        logi(msg)


def logs(timestamp: AlarmTime, state: ST, msg: str = ''):
    if IS_DEBUG  or  msg.startswith('ERROR'):
        print(f'{timestamp} - {state}: {msg}')
        sys.stdout.flush()
        sys.stderr.flush()
        print(f'{timestamp} - {state}: {msg}', file=g_logFile)
        g_logFile.flush()


def createSoundController():
    """
    Use this factory function instead of ctors directly
    to make switch between types of sound easy.
    """
    if True:
        return PygameSoundController()
        # return OmxplayerController()
    else:
        return PwmController()


def init_GPIO():
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BOARD)
    GPIO.setup(SWITCH_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)

    GPIO.setup(LED_RED_PIN, GPIO.OUT)
    GPIO.setup(LED_GREEN_PIN, GPIO.OUT)
    GPIO.setup(LED_YELLOW_PIN, GPIO.OUT)

    # turn LEDs off
    GPIO.output(LED_RED_PIN, 0)
    GPIO.output(LED_GREEN_PIN, 0)
    GPIO.output(LED_YELLOW_PIN, 0)
    
    GPIO.setup(SOUND_AMPLIFIER_ON_OFF_PIN, GPIO.OUT)
    GPIO.output(SOUND_AMPLIFIER_ON_OFF_PIN, 1)

    logi("GPIO initialized")


def init_scale():
    hx = HX711()
    hx.set_reference_unit(SCALE_REFERENCE_UNIT)
    # hx.reset()
    logi("Scale initialized, measuring tare ...")
    hx.tare()
    logi("Scale ready")
    return hx


def read_weight(hx: HX711):

    val = hx.get_weight(5)
    logi(f' weight = {val},  value - tare = {hx.pop_log_data()}')

    time.sleep(0.1)

    return val


def is_switch_pressed() -> bool:
    return GPIO.input(SWITCH_PIN) == 0


def set_led(led, onoff):
    GPIO.output(led, onoff)


def set_all_leds(on):
    GPIO.output(LED_RED_PIN, on)
    GPIO.output(LED_GREEN_PIN, on)
    GPIO.output(LED_YELLOW_PIN, on)


def test_player_controller():
    pc = createSoundController()
    pc.play_alarm()
    time.sleep(3)
    pc.stop_alarm()
    time.sleep(1)
    # 
    pc.play_alarm()
    time.sleep(1)
    pc.play_alarm()  # this one should not start another player
    time.sleep(3)
    pc.stop_alarm()


def test_hardware(hx):
    print("Leds should be blinking synchronously, press switch to continue ...")
    while not is_switch_pressed():
        GPIO.output(LED_RED_PIN, 1)
        GPIO.output(LED_GREEN_PIN, 1)
        GPIO.output(LED_YELLOW_PIN, 1)
        time.sleep(1)
        if is_switch_pressed(): break
        GPIO.output(LED_RED_PIN, 0)
        GPIO.output(LED_GREEN_PIN, 0)
        GPIO.output(LED_YELLOW_PIN, 0)
        time.sleep(1)

    # start next step with all LEDs off
    set_all_leds(0)

    while is_switch_pressed():
        time.sleep(0.5)

    print("\nLeds are blinking in sequence, press switch to continue ...")
    while not is_switch_pressed():
        GPIO.output(LED_RED_PIN, 1)
        time.sleep(1)
        if is_switch_pressed(): break
        GPIO.output(LED_RED_PIN, 0)
        GPIO.output(LED_GREEN_PIN, 1)
        time.sleep(1)
        if is_switch_pressed(): break
        GPIO.output(LED_GREEN_PIN, 0)
        GPIO.output(LED_YELLOW_PIN, 1)
        time.sleep(1)
        GPIO.output(LED_YELLOW_PIN, 0)

    set_all_leds(0)

    print('Put objects on scale and read their weight, press switch to continue ...')
    while is_switch_pressed():
        time.sleep(0.5)

    while not is_switch_pressed():
        print(read_weight(hx), 'g')

    while is_switch_pressed():
        time.sleep(0.5)

    print('Alarm should be on, then stopped, then on again')
    test_player_controller()

    print('Press switch to end test ...')
    pc = createSoundController()

    while not is_switch_pressed():
        pc.play_alarm()
        time.sleep(0.5)

    pc.stop_alarm()

    print("Test ended ...")


def test_scale(hx: HX711):
    print("Press switch to exit ...")
    while not is_switch_pressed():
        print(read_weight(hx))


def play_alarm_once():
    pc = createSoundController()
    pc.play_alarm()
    while pc.is_alarm_playing()  and not is_switch_pressed():
        time.sleep(0.2)

    pc.stop_alarm()  # always turn off amplifier


def play_alarm():
    pc = createSoundController()
    startTime = time.time()
    while not is_switch_pressed()  and  time.time() - startTime < MAX_ALARM_DURATION_S:
        pc.play_alarm()
        time.sleep(0.5)
    pc.stop_alarm()

    # if there is timeout - nobody pressed the switch, wait until
    # sombedy marks his presence. Otherwise alarm has no sense.
    if time.time() - startTime >= MAX_ALARM_DURATION_S:
        log("Press switch for at least 0.5 s to return to normal program operation!")
        while not is_switch_pressed():
            time.sleep(0.4)
            set_all_leds(True)
            time.sleep(0.4)
            set_all_leds(False)


def set_LEDs(timestamp: AlarmTime, state: ST, is_error: bool):

    set_led(LED_RED_PIN, is_error)

    if state == ST.BEFORE_CALIBRATION:
        set_led(LED_GREEN_PIN, False)
        set_led(LED_YELLOW_PIN, True)
        time.sleep(0.2)
        set_led(LED_YELLOW_PIN, False)
        if g_starchMode == ac.STARCH_MODE_GLYCOSADE:
            set_led(LED_GREEN_PIN, True)
            time.sleep(0.05)
            set_led(LED_GREEN_PIN, False)

    elif state == ST.CALIBRATED or state == ST.GLASS_NOT_ON_SCALE_ERROR:
        set_led(LED_GREEN_PIN, True)
        time.sleep(0.2)
        set_led(LED_GREEN_PIN, False)
        if g_starchMode == ac.STARCH_MODE_GLYCOSADE:
            set_led(LED_YELLOW_PIN, True)
            time.sleep(0.05)
            set_led(LED_YELLOW_PIN, False)

    elif state == ST.GLASS_WITH_CS_ON: 
            set_led(LED_YELLOW_PIN, True)

    elif state == ST.GLASS_WITH_CS_MAY_BE_TAKEN_OFF:
            set_led(LED_YELLOW_PIN, False)
            set_led(LED_GREEN_PIN, True)

    elif state == ST.GLASS_WITH_CS_OFF:
        set_led(LED_GREEN_PIN, True)
        set_led(LED_YELLOW_PIN, True)
        time.sleep(0.3)
        set_led(LED_GREEN_PIN, False)
        set_led(LED_YELLOW_PIN, False)

    elif state == ST.GLASS_EMPTY_ON:
        set_led(LED_GREEN_PIN, True)
        set_led(LED_YELLOW_PIN, True)


def readCSModeFromFile():
    """
    Reads CS mode from file written by bluertoothServer, which receives it 
    from mobile app.
    """
    try:  # prevent file handling to terminate the program
        if os.path.exists(ac.SETTINGS_IN_FILE):
            with open(ac.SETTINGS_IN_FILE, 'r') as inf:
                # this file should contain one line only, describing CS mode
                csMode = inf.readline()
                log("CS mode will be set to: " + csMode)
                if csMode.startswith(ac.STARCH_MODE_GLYCOSADE):
                    # do not write to file if not necessary
                    if g_starchMode != ac.STARCH_MODE_GLYCOSADE:
                        setTimesForStarch(ac.STARCH_MODE_GLYCOSADE)
                        writeConfigToFile()
                else:
                    if g_starchMode != ac.STARCH_MODE_CORNSTARCH:
                        setTimesForStarch(ac.STARCH_MODE_CORNSTARCH)
                        writeConfigToFile()

            # remove the file to avoid redundant writing of time to out file
            os.remove(ac.SETTINGS_IN_FILE)

    except Exception as ex:
        logs(now, state, "ERROR: Can't read settings in file with CS mode: " + str(ex))


def alarm_scale(hx: HX711):
    """
    This is the main function for alarm scale.
    """
    testTick = 0
    
    state = ST.BEFORE_CALIBRATION
    is_alaram_played_once = False
    is_error = False

    while True:

        now = dtm.datetime.now()
        now = AlarmTime(now.hour, now.minute)

        if IS_TEST_MODE:
            testTick += 1
            testIdx = testTick // 3 # switch to next test time every N cycles
            if testIdx >= len(TEST_TIMES):
                testIdx = testTick = 0
            now = TEST_TIMES[testIdx]

        for repeat in range(2):
            set_LEDs(now, state, is_error)
            time.sleep(1)  # cycle time

        logs(now, state, f'---------------- w = {read_weight(hx)}')

        readCSModeFromFile()

        if now >= START_OF_DAY_TIME  and  now < CALIBRATION_TIME:
            state = ST.BEFORE_CALIBRATION
            is_alaram_played_once = False
            is_error = False
            logs(now, state, 'in idle loop during the day')
            continue

        # state machine is run only during the night 
        if state == ST.BEFORE_CALIBRATION:
            current_weight = read_weight(hx)
            if current_weight > MAX_WEIGHT_ON_EMPTY_SCALE_g:
                if not is_alaram_played_once:
                    logs(now, state, f'ERROR: Something is already on scale! weight = {current_weight}')
                    play_alarm_once()  
                    is_alaram_played_once = True
                is_error = True
                logs(now, state, f'ERROR: Calibration Error, weight = {current_weight}')
            else:
                hx.tare()  # calibrate once per day
                state = ST.CALIBRATED
                is_error = False
                is_alaram_played_once = False # allow alarm to be played once again at other errors
                logs(now, state, "OK: Scale has performed daily calibration. You may put on glass with CS.")

        elif state == ST.CALIBRATED  or  state == ST.GLASS_NOT_ON_SCALE_ERROR:
            # get weight of glass with CS
            current_weight = read_weight(hx)
            if current_weight > MAX_WEIGHT_ON_EMPTY_SCALE_g:
                logs(now, state, f'current_weight = {current_weight}')
                state = ST.GLASS_WITH_CS_ON
                cs_and_glass_weight = 0
                num_additions = 3
                for i in range(num_additions):
                    time.sleep(1)  # wait until the weight settles
                    w = read_weight(hx)
                    cs_and_glass_weight += w
                    logs(now, state, f'cs_and_glass_weight = {w}')

                cs_and_glass_weight /= num_additions  # get average
                logs(now, state, f'cs_and_glass_weight avg = {cs_and_glass_weight}')

                is_error = False  # if glass was tken off scale or not put on 
                                  # scale on time, then now we are back in valid
                                  # state

                if cs_and_glass_weight < MAX_WEIGHT_ON_EMPTY_SCALE_g:
                    logs(now, state, 'ERROR: Weight of glass with CS is too low!'
                            f"cs_and_glass_weight = {cs_and_glass_weight}")
                    state = ST.GLASS_NOT_ON_SCALE_ERROR  # user should put CS back
                    is_error = True
                    play_alarm()  

                log(f"Keep CS on scale until: {CS_MUST_BE_ON_SCALE_END} but take it off and empty back at "
                        + str(EMPTY_GLASS_IS_BACK_ON_SCALE_TIME) + " latest.")

        elif state == ST.GLASS_WITH_CS_ON:
            if now > CALIBRATION_TIME  or  now <= CS_MUST_BE_ON_SCALE_END:
                current_weight = abs(read_weight(hx))
                if abs(current_weight - cs_and_glass_weight) > WEIGHT_EPSILON_g:
                    logs(now, state, 'ERROR: Somebody moved CS off scale too early!'
                            f"cs_and_glass_weight = {cs_and_glass_weight}, "
                            f"current_weight = {current_weight}, diff = {cs_and_glass_weight - current_weight}")
                    state = ST.GLASS_NOT_ON_SCALE_ERROR  # user should put CS back
                    is_error = True
                    play_alarm()  
            else:
                state = ST.GLASS_WITH_CS_MAY_BE_TAKEN_OFF

        elif state == ST.GLASS_WITH_CS_MAY_BE_TAKEN_OFF:
                # Check if it is later than time for empty glass on scale.
                # The second condition prevents alarm in the evening.
                if now >= EMPTY_GLASS_IS_BACK_ON_SCALE_TIME and now < START_OF_DAY_TIME:
                    logs(now, state, 'ERROR: CS was NOT taken off scale!')
                    is_error = True
                    play_alarm() 

                current_weight = abs(read_weight(hx))
                if  current_weight < WEIGHT_EPSILON_g:
                    state = ST.GLASS_WITH_CS_OFF
                    logs(now, state, 'OK:    Glass with CS was taken off scale. Put empty glass back until '
                                     + str(EMPTY_GLASS_IS_BACK_ON_SCALE_TIME))
                else:
                    log(f"Glass may be taken off. weight = {current_weight}")

        elif state == ST.GLASS_WITH_CS_OFF:
            glass_weight = read_weight(hx)
            if abs(cs_and_glass_weight - glass_weight - CS_WEIGHT_g) <= WEIGHT_EPSILON_g:
                log("OK, empty glass was put back on scale.")
                state = ST.GLASS_EMPTY_ON
            if now > EMPTY_GLASS_IS_BACK_ON_SCALE_TIME:
                logs(now, state, f"ERROR: Glass has not been put back or not "
                        f"all CS was consumed: cs_and_glass_weight = {cs_and_glass_weight}, "
                        f"CS_WEIGHT_g = {CS_WEIGHT_g}, current_weight = {glass_weight}")
                play_alarm()  
                is_error = True

        elif state == ST.GLASS_EMPTY_ON:
            pass # nothing to do in this state. Even if glass is taken off, don't care
        else:
            # don't throw exception - it still better to continue than to crash
            logs(now, state, "Unhandled state!")
            play_alarm()  
            is_error = True

        # check that glass with CS is put on scale before limit time
        if (now > CS_MUST_BE_ON_SCALE_START  and  state != ST.GLASS_WITH_CS_ON 
                and state != ST.GLASS_NOT_ON_SCALE_ERROR):  # this condition allows 
                 # to reset state machine if glass with CS is not put on scale on time

            if not is_alaram_played_once:
                logs(now, state, f'ERROR: There is no glass with CS on scale at '
                                 f'expected time: {CS_MUST_BE_ON_SCALE_START}')
                play_alarm_once()
                is_alaram_played_once = True

            state = ST.GLASS_NOT_ON_SCALE_ERROR  # user should put CS on scale
            is_error = True


def configure_test_mode():
    global IS_TEST_MODE, MAX_ALARM_DURATION_S, TEST_TIMES

    IS_TEST_MODE = True
    MAX_ALARM_DURATION_S = 30  # 30 seconds
    TEST_TIMES = [START_OF_DAY_TIME.prev(), START_OF_DAY_TIME, START_OF_DAY_TIME.next(),
             CALIBRATION_TIME.prev(), CALIBRATION_TIME, CALIBRATION_TIME.next(), 
             CS_MUST_BE_ON_SCALE_START.prev(), CS_MUST_BE_ON_SCALE_START, CS_MUST_BE_ON_SCALE_START.next(), 
             CS_MUST_BE_ON_SCALE_END.prev(), CS_MUST_BE_ON_SCALE_END, CS_MUST_BE_ON_SCALE_END.next(),
             EMPTY_GLASS_IS_BACK_ON_SCALE_TIME.prev(), EMPTY_GLASS_IS_BACK_ON_SCALE_TIME, 
             EMPTY_GLASS_IS_BACK_ON_SCALE_TIME.next()]


def parse_args():
    parser = argparse.ArgumentParser(description='Alarm scale controller.')
    parser.add_argument('-w', '--hwtest', dest='is_hw_test', action='store_true',
                        default=False, 
                        help='If specified, interactive hardware test is run. '
                        'User has to follow instructions on stdout.')
    parser.add_argument('-t', '--swtest', dest='is_sw_test', action='store_true',
                        default=False, 
                        help='If specified, interactive software test is run. '
                        'User has to put glass with CS on/off scale. See test ' 
                        'descriptions in doc dir.')
    parser.add_argument('-s', '--scale', dest='is_scale_test', 
                        action='store_true', default=False, 
                        help='If specified, weights measured by scale are printed.')

    args = parser.parse_args()
    return args


def main():

    openLog()

    cornStarch = readConfigFromFile()
    setTimesForStarch(cornStarch)

    logi(f"START_OF_DAY_TIME = {START_OF_DAY_TIME}, CALIBRATION_TIME = {CALIBRATION_TIME}\n"
          f"CS_MUST_BE_ON_SCALE_START = {CS_MUST_BE_ON_SCALE_START},  CS_MUST_BE_ON_SCALE_END = {CS_MUST_BE_ON_SCALE_END},\n"
          f"EMPTY_GLASS_IS_BACK_ON_SCALE_TIME = {EMPTY_GLASS_IS_BACK_ON_SCALE_TIME}")

    args = parse_args()

    init_GPIO()
    hx = init_scale()
    if args.is_hw_test:
        test_hardware(hx)
    elif args.is_scale_test:
        test_scale(hx)
    elif args.is_sw_test:
        configure_test_mode()
        alarm_scale(hx)
    else:
        alarm_scale(hx)

if __name__ == '__main__':
    main()
    GPIO.cleanup()

