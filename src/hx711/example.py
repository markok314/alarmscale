#! /usr/bin/python2

# This file show example of usage for hx711 module. It is not used in
# final application.
#
# This file was originally copied from https://github.com/tatobari/hx711py.

import time
import sys
from hx711 import HX711

hx = HX711()

# HOW TO CALCULATE THE REFFERENCE UNIT
# To set the reference unit to 1. Put 1kg on your sensor or anything you have and know exactly how much it weights.
# In this case, 92 is 1 gram because, with 1 as a reference unit I got numbers near 0 without any weight
# and I got numbers around 184000 when I added 2kg. So, according to the rule of thirds:
# If 2000 grams is 184000 then 1000 grams is 184000 / 2000 = 92.

referenceUnit = 565000 / 500  # value at 500g weight
hx.set_reference_unit(referenceUnit)

hx.tare()

print("Tare done! Add weight now...")

#hx.tare_A()
counter = 0
measurementNo = 0
while True:
    try:
        # These three lines are usefull to debug wether to use MSB or LSB in the reading formats
        # for the first parameter of "hx.set_reading_format("LSB", "MSB")".
        # Comment the two lines "val = hx.get_weight(5)" and "print val" and 
        # uncomment these three lines to see what it prints.
        
        # np_arr8_string = hx.get_np_arr8_string()
        # binary_string = hx.get_binary_string()
        # print binary_string + " " + np_arr8_string
        
        # Prints the weight. Comment if you're debbuging the MSB and LSB issue.
        val = hx.get_weight(1)
        # print(val)
        if abs(val) > 0:
            counter += 1
            print('counter =', counter, val, measurementNo, hx.lastVal) #, bin(hx.lastVal))

        measurementNo += 1
        if measurementNo % 1000 == 0:
            print("measurementNo = ", measurementNo)

        time.sleep(1)

        # To get weight from both channels (if you have load cells hooked up 
        # to both channel A and B), do something like this
        #val_A = hx.get_weight_A(5)
        #val_B = hx.get_weight_B(5)
        #print "A: %s  B: %s" % ( val_A, val_B )

    except (KeyboardInterrupt, SystemExit) as ex:
        print("Exception: ", ex)
        hx.terminate_reader_thread();
        break

