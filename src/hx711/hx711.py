# This module reads values from HX711 AD converter used for
# reading force (usually weight) on Load Cell.
#
# This file was originally copied from https://github.com/tatobari/hx711py.

import time
import threading
from queue import Queue
import subprocess as sp

IS_LOG_ON = False


class HX711:

    def __init__(self):

        # The value returned by the hx711 that corresponds to your reference
        # unit AFTER dividing by the SCALE.
        self.REFERENCE_UNIT = 1
        self.REFERENCE_UNIT_B = 1

        self.offset = 1
        self.offset_B = 1
        self.lastVal = int(0)
        self.logData = ''

        self.DEBUG_PRINTING = False

        self.process_hx711 = sp.Popen('/home/pi/alarmscale/src/hx711/hx711', stdout=sp.PIPE)
        self.weights_queue = Queue()
        self.reader_thread = threading.Thread(name='hx711ReaderThread', 
                                              target=self.read_hx711, 
                                              args=[self.weights_queue])
        self.is_reader_thread_terminated = False
        self.reader_thread.start()
        
    def read_hx711(self, weights_queue: Queue):
        while (not self.is_reader_thread_terminated):
            weight = self.process_hx711.stdout.readline().strip()
            if weight.isdigit():
                weights_queue.put_nowait(int(weight))
            else:
                if not weight:
                    print("No data. Check if background process is still running and occupying hx711 port?")
                else:
                    print(f"Read from HX711: {weight}, is_reader_thread_terminated: {self.is_reader_thread_terminated}")
                time.sleep(0.5)

    def terminate_reader_thread(self):
        self.is_reader_thread_terminated = True
        self.process_hx711.terminate()


    def pop_log_data(self):
        t = self.logData
        self.logData = ''
        return t

    def convertFromTwosComplement24bit(self, inputValue):
        return -(inputValue & 0x800000) + (inputValue & 0x7fffff)
    
    def read_long(self):

        twosComplementValue = self.weights_queue.get(block=True)
        # avoid mem leak in case of slow consumer
        while not self.weights_queue.empty():
            self.weights_queue.get(block=True)
            # print("Queue cleanup, because consumer is slow!")


        if self.DEBUG_PRINTING:
            print("Twos: 0x%06x" % twosComplementValue)
        
        # Convert from 24bit twos-complement to a signed value.
        signedIntValue = self.convertFromTwosComplement24bit(twosComplementValue)

        # Record the latest sample value we've read to  ba available for debugging.
        self.lastVal = signedIntValue 
        self.logData = str(signedIntValue - self.offset)

        # Return the sample value we've read from the HX711.
        return signedIntValue

    
    def read_average(self, times=3):
        # Make sure we've been asked to take a rational amount of samples.
        if times <= 0:
            raise ValueError("HX711()::read_average(): times must >= 1!!")

        # If we're only average across one value, just read it and return it.
        if times == 1:
            return self.read_long()

        # If we're averaging across a low amount of values, just take the
        # median.
        if times < 5:
            return self.read_median(times)

        # If we're taking a lot of samples, we'll collect them in a list, remove
        # the outliers, then take the mean of the remaining set.
        valueList = []

        for x in range(times):
            valueList += [self.read_long()]

        valueList.sort()

        # We'll be trimming 20% of outlier samples from top and bottom of collected set.
        trimAmount = int(len(valueList) * 0.2)

        # Trim the edge case values.
        valueList = valueList[trimAmount:-trimAmount]

        # Return the mean of remaining samples.
        return sum(valueList) / len(valueList)


    # A median-based read method, might help when getting random value spikes
    # for unknown or CPU-related reasons
    def read_median(self, times=3):
       if times <= 0:
          raise ValueError("HX711::read_median(): times must be greater than zero!")
      
       # If times == 1, just return a single reading.
       if times == 1:
          return self.read_long()

       valueList = []

       for x in range(times):
          valueList += [self.read_long()]

       valueList.sort()

       # If times is odd we can just take the centre value.
       if (times & 0x1) == 0x1:
          return valueList[len(valueList) // 2]
       else:
          # If times is even we have to take the arithmetic mean of
          # the two middle values.
          midpoint = len(valueList) / 2
          return sum(valueList[midpoint:midpoint+2]) / 2.0


    # Compatibility function, uses channel A version
    def get_value(self, times=3):
        return self.get_value_A(times)


    def get_value_A(self, times=3):
        return self.read_median(times) - self.get_offset_A()


    def get_value_B(self, times=3):
        # for channel B, we need to set_gain(32)
        g = self.get_gain()
        self.set_gain(32)
        value = self.read_median(times) - self.get_offset_B()
        self.set_gain(g)
        return value

    # Compatibility function, uses channel A version
    def get_weight(self, times=3):
        return self.get_weight_A(times)


    def get_weight_A(self, times=3):
        value = self.get_value_A(times)
        value = value / self.REFERENCE_UNIT
        return value

    def get_weight_B(self, times=3):
        value = self.get_value_B(times)
        value = value / self.REFERENCE_UNIT_B
        return value

    
    # Sets tare for channel A for compatibility purposes
    def tare(self, times=15):
        self.tare_A(times)
    
    
    def tare_A(self, times=15):
        # Backup REFERENCE_UNIT value
        backupReferenceUnit = self.get_reference_unit_A()
        self.set_reference_unit_A(1)
        
        value = self.read_average(times)

        if self.DEBUG_PRINTING:
            print("Tare A value:", value)
        
        self.set_offset_A(value)

        # Restore the reference unit, now that we've got our offset.
        self.set_reference_unit_A(backupReferenceUnit)

        return value


    def tare_B(self, times=15):
        # Backup REFERENCE_UNIT value
        backupReferenceUnit = self.get_reference_unit_B()
        self.set_reference_unit_B(1)

        # for channel B, we need to set_gain(32)
        backupGain = self.get_gain()
        self.set_gain(32)

        value = self.read_average(times)

        if self.DEBUG_PRINTING:
            print("Tare B value:", value)
        
        self.set_offset_B(value)

        # Restore gain/channel/reference unit settings.
        self.set_gain(backupGain)
        self.set_reference_unit_B(backupReferenceUnit)
       
        return value

    # sets offset for channel A for compatibility reasons
    def set_offset(self, offset):
        self.set_offset_A(offset)

    def set_offset_A(self, offset):
        self.offset = offset

    def set_offset_B(self, offset):
        self.offset_B = offset

    def get_offset(self):
        return self.get_offset_A()

    def get_offset_A(self):
        return self.offset

    def get_offset_B(self):
        return self.offset_B


    def set_reference_unit(self, reference_unit):
        self.set_reference_unit_A(reference_unit)

        
    def set_reference_unit_A(self, reference_unit):
        # Make sure we aren't asked to use an invalid reference unit.
        if reference_unit == 0:
            raise ValueError("HX711::set_reference_unit_A() can't accept 0 as "
                             "a reference unit!")
            return

        self.REFERENCE_UNIT = reference_unit

    def set_reference_unit_B(self, reference_unit):
        # Make sure we aren't asked to use an invalid reference unit.
        if reference_unit == 0:
            raise ValueError("HX711::set_reference_unit_A() can't accept 0 as a reference unit!")
            return

        self.REFERENCE_UNIT_B = reference_unit


    def get_reference_unit(self):
        return get_reference_unit_A()

        
    def get_reference_unit_A(self):
        return self.REFERENCE_UNIT

        
    def get_reference_unit_B(self):
        return self.REFERENCE_UNIT_B
        
        
def main():
    hx711 = HX711()
    time.sleep(5)

if __name__ == '__main__':
    main()

