#include <iostream>
#include <exception>
#include <unistd.h>
#include <system_error>
#include <sys/time.h>

#include "gpiod.hpp"

using std::cout;
using std::endl;
using std::invalid_argument;


// These are indices of line returned by gpiod_ship_get_line. Hopefully
// these will rmeain constant in the future version of the library. 
// There is no official doc linking line indices to RPi header pin numbers.
constexpr int SCALE_DT_PIN = 5; // header pin 29
constexpr int SCALE_SCK_PIN = 6; // header pin 31

gpiod_chip *chip;
struct gpiod_line *scale_dout_pin;
struct gpiod_line *scale_sck_pin;


void init(int dout_gpio_pin, int pd_sck_gpio_pin) {

    chip = gpiod_chip_open("/dev/gpiochip0");
    if (!chip) {
        throw std::invalid_argument("Can not open gpio chip.");
    }

    scale_dout_pin = gpiod_chip_get_line(chip, SCALE_DT_PIN);
    scale_sck_pin = gpiod_chip_get_line(chip, SCALE_SCK_PIN);

    if (scale_dout_pin == nullptr || scale_sck_pin == nullptr) {
        throw std::invalid_argument("Can not get chip lines.");
    }
    if (gpiod_line_request_output(scale_sck_pin, "scale-clock", 0) != 0) {
        throw std::invalid_argument("Can not allocate requested output.");
    }
    if (gpiod_line_request_input(scale_dout_pin, "scale-out") != 0) {
        throw std::invalid_argument("Can not allocate requested input.");
    }
}


/**
 * Makes program execute faster (about 2.7ms for one read cycle vs 4ms with
 * normal scheduler. However, if cout is used, the timing becommes much more 
 * variable with FIFO scheduler, so I decided for normal scheduler.
 * Normal scheduler also has sometimes high SCK pulses much longer than most 
 * of them, but they are still below 50us (measured 42 us).
 * When scheduler is set, you have to set permissions with:
 *
 *     sudo setcap cap_sys_nice+ep hx711
 */
void setScheduler() {
    sched_param schedParam;
    schedParam.sched_priority = sched_get_priority_max(SCHED_FIFO);
    int retVal = sched_setscheduler(0, SCHED_FIFO, &schedParam);
    if (retVal == -1) {
        throw std::system_error(errno, std::system_category(), "Can not set scheduler");
    }
}


void toggle_sck() {
    if (gpiod_line_set_value(scale_sck_pin, 1) == -1) {
        throw std::invalid_argument("Can not set SCK to high!");
    }
    // without sleep pulse time is about 2.7 us. With this usleep(1) call
    // it increases to more than 100us, which puts HX711 to power down mode.
    // usleep(1);  // SCK high time 0.2us..50us
    if (gpiod_line_set_value(scale_sck_pin, 0) == -1) {
        throw std::invalid_argument("Can not set SCK to low!");
    }
}



unsigned long readCount(void) {
    unsigned long count = 0;

    // repeat loop if measurement time was too long, which means something 
    // in OS (process switch) interrupted measurement, which makes it 
    // invalid (clock high longer than 50 us).
    while (true) {

        // wait for scale to indicate avilable conversion value
        int scale_dout_value = 0;
        do {
            scale_dout_value = gpiod_line_get_value(scale_dout_pin);
            if (scale_dout_value == -1) {
                throw std::invalid_argument("Can not read value of scale DOUT pin!");
            }
        } while(scale_dout_value);

        struct timeval tv;
        gettimeofday(&tv, nullptr);
        unsigned long start_time =  tv.tv_usec + tv.tv_sec * 1'000'000;
        // read bits
        for (unsigned i = 0; i < 24; i++){

    //        usleep(1);  // DOUT falling edge to SCK rising edge, min 0.1us, max undefined

            toggle_sck();
            scale_dout_value = gpiod_line_get_value(scale_dout_pin);

            if (scale_dout_value == -1) {
                throw std::invalid_argument("Can not read value of scale DOUT pin!");
            }

    //        usleep(1);  // SCK low time 0.2us..inf

            count = count << 1;
            if (scale_dout_value) {
                count++;
            }
        }

        toggle_sck();  // get 25th bit, but ignore it.

        gettimeofday(&tv, nullptr);
        unsigned long end_time =  tv.tv_usec + tv.tv_sec * 1'000'000;
        // It seems the ost time is spent in libgpiod. g++ switches -O0, -O1, -O2
        // do not seem to have much influence. Values measured by oscilloscope:
        // high ~= 2us, low ~= 4us, one read cyle (25 bits) ~= 150 us. Since one
        // clock high must be below 50 us, time greater than 200 ms means something 
        // went wrong.
        if (end_time - start_time < 250) {
            break;
        }
        count = 0;
        cout << "overflow: " << (end_time - start_time) << endl;
    }

    return count;
}


int main() {
    try {
        init(SCALE_DT_PIN, SCALE_SCK_PIN);
        // setScheduler();  see comment of the function why this is disabled
                
        while (true) {
            cout << readCount() << endl;
        }
    } catch (std::exception &ex) {
        std::cerr << "ERROR: " << ex.what() << endl;
    }
}

