# This file contains constants common to server and alarmscale.

SETTINGS_OUT_FILE = '/tmp/settings.out'  # output to phone
SETTINGS_IN_FILE = '/tmp/settings.in'  # input to raspi

CONFIG_FILE = 'alarm.config'

# these two should be the same as enum StarchModes in mobile app.
STARCH_MODE_GLYCOSADE = 'Glycosade'
STARCH_MODE_CORNSTARCH = 'CornStarch'
