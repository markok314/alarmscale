#!/usr/bin/python

# This script starts bluetooth server and waits for requests from mobile
# app. Data received from mobile app is saved to file, where it is used
# by alarmScaleMain.py. It also reads file produced by alarmScaleMain.py
# and returns it to the mobile client.
# IDs:
#   - tehtnica (RPi)      : B8:27:EB:C4:6D:88
#   - bager               : 00:1A:7D:DA:71:13
#   - my phone seems to be: F4:F5:24:2E:DA:2B
#   - moto g(8) power     : F8:1F:32:CD:1C:DF

import bluetooth as bt
import ntp
import logging as log
import subprocess as sp
import sys
import os
import traceback
import alarm_consts as ac
import time
import yaml

# Client sample
# def client_sample():
#     bd_addr = "20:13:05:30:01:14"
#     port = 1
#     sock = bt.BluetoothSocket(bt.RFCOMM)
#     sock.connect((bd_addr, port))
#     sock.send('\x1A')

# file: rfcomm-server.py
# auth: Albert Huang <albert@csail.mit.edu>, Marko Klopcic, markok3.14@gmail.com
# desc: simple demonstration of a server application that uses RFCOMM sockets

# these should be the same as in mobile app.
YAML_KEY_COMMAND = 'command'  # see YAML_KEY_* strings below for valid commands
YAML_KEY_TIME = 'time'
YAML_KEY_CS_MODE = 'csMode'

CMD_SET_TIME = 'setTime'       # must be followed by YAML_KEY_TIME
CMD_SET_CS_MODE = 'setCsMode'  # must be followed by YAML_KEY_CS_MODE
CMD_GET_STATUS = 'getStatus'

TIME_YAML_KEY = 'time'  # sent to mobile app


def _init():
    # Without this line piscan mode for hci0 is not set below. When this f. is called 
    # again from exception handler, scan mode is set, mobile app connects, but
    # data transfer does not work. So I tried timeout instead of this call, and
    # 3 seconds were OK, 1 second not. I left this call followed by timeout,
    # to be sure.
    sp.check_call(['hciconfig'])
    log.info("Now setting piscan ...")
    # there are sometimes problems on connect, usually sleep(3) is OK, but not always
    for i in range(5):         
        time.sleep(3)
        try:
            # This makes raspi bluetooth discoverable. Otherwise phone can't connect.
            sp.check_call(['sudo', 'hciconfig', 'hci0', 'piscan'])
            log.info(f"piscan started on atempt no {i}")
            break;
        except sp.CalledProcessError as ex:
            log.info(f"WARNING: piscan start FAILED on atempt no {i}")
    else:
        raise Exception("FATAL: piscan start failed, aborting!")

    # This allows running this script without sudo. User pi must be added 
    # to group bluetooth.
    sp.check_call(['sudo', 'chgrp', 'bluetooth', '/var/run/sdp'])


def parseReceivedString(yamlFromMobile: str):
    """
    String from mobile:
        command: <see CMD_* strings above for commands>
        <param>: <see YAML_KEY_* strings above>

        @return: [command, mapping]
    """
    dataFromMobile = yaml.safe_load(yamlFromMobile)
    return dataFromMobile[YAML_KEY_COMMAND], dataFromMobile


def readAlarmScaleSettings() -> str:
    with open(ac.SETTINGS_OUT_FILE, 'r') as inf:
        lines = inf.readlines()

    return "".join(lines)


def waitForStatusUpdate(prevFileModificationTime, fpath):
    """
    Waits until module alarmScaleMain.py updates current status in 
    output file.
    """
    while (True):
        fileModificationTime = os.path.getmtime(fpath)
        if fileModificationTime != prevFileModificationTime:
            break
        time.sleep(0.2)


def openConnection():
    # This code is taken from sample, but there no doc about the meaning of parameters.
    server_sock = bt.BluetoothSocket(bt.RFCOMM)
    server_sock.bind(("", bt.PORT_ANY))
    server_sock.listen(1)

    port = server_sock.getsockname()[1]

    uuid = "94639d29-7ddd-4a7d-9c3b-fba39e4aaf45"

    bt.advertise_service(server_sock, "SampleServer",
                         service_id=uuid,
                         service_classes=[uuid, bt.SERIAL_PORT_CLASS],
                         profiles=[bt.SERIAL_PORT_PROFILE],
                         #                   protocols = [ OBEX_UUID ]
                        )
    return server_sock


def accept(server_socket):
    log.info("Waiting for connection on RFCOMM channel")

    client_sock, client_info = server_socket.accept()
    log.info("Accepted connection from " + str(client_info))

    try:
        while True:
            data = client_sock.recv(1024).decode('utf8')
            if len(data) == 0:
                break
            try:
                log.debug(f"received {data}")
                cmd, mobileSettings = parseReceivedString(data)

                if cmd == CMD_SET_TIME:
                    ntp.setTimeManually(mobileSettings[YAML_KEY_TIME])
                elif cmd == CMD_SET_CS_MODE:
                    settingsModTime = os.path.getmtime(ac.SETTINGS_OUT_FILE)
                    with open(ac.SETTINGS_IN_FILE, 'w') as outf:
                        outf.write(mobileSettings[YAML_KEY_CS_MODE])
                    waitForStatusUpdate(settingsModTime, ac.SETTINGS_OUT_FILE)
                elif cmd == CMD_GET_STATUS:
                    pass  # standard response is set below
                else:
                    log.error("Invalid command received from mobile app: " + data)

                response = readAlarmScaleSettings()
                response += f"{TIME_YAML_KEY}: {ntp.currentTime()}\n"
                log.debug('Sending response: ' + response)
                client_sock.send(response)

            except Exception as ex:
                log.error("Error receiving/sending data! " + str(ex))
                
    except IOError:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        log.error("Error receiving/sending data!")
        traceback.print_exception(exc_type, exc_value, exc_traceback,
                                  limit=2, file=sys.stdout)
        log.debug("")  # flush the data

    log.info("disconnected")

    client_sock.close()
    server_socket.close()


def main():

    log.basicConfig(level=log.DEBUG, format='%(asctime)s %(message)s')

    _init()
    if not ntp.isNtpActive(): # always make sure NTP is active on start. 
        ntp.setNtpActivity(True)  # When user sets time from phone, it is turned off.

    while True:
        try:
            server_socket = openConnection()
            accept(server_socket)
        except Exception:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            log.error("Error accepting connection!")
            traceback.print_exception(exc_type, exc_value, exc_traceback,
                                      limit=2, file=sys.stdout)
            log.debug("")  # flush the data
            # it seems that when started by cron during boot, scan mode is not
            # set during first call to _init(). Use:  $ hciconfig  to see scan
            # mode set for device.
            # Untested alternative: /etc/udev/rules.d/10-local.rules:
            # Set bluetooth power up
            # ACTION=="add", KERNEL=="hci0", RUN+="/usr/bin/hciconfig hci0 up"
            log.info("Calling _init() again ...")
            time.sleep(3)
            _init()


if __name__ == "__main__":
    main()

