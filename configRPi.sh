# This file configures new RPi zero W to be ready for alarm scale.
# 1. Install *-raspbian-*-lite.zip. Desktop is not needed, although
#    installation of omxplayer below then takes some time.
# 2. Run 
#      sudo raspi-config
#    and set new password and hostname to 'tehtnica'. If you set
#    hostname manually, change host name in /etc/hostname and /etc/hosts.
#    Configure also other items, like keyboard, networking, ...
# 3. On PC:
#      cd ~
#      scp .vimrc .bash_aliases .gitconfig pi@tehtnica:
# 4. Add hotspot wifi network from phone by copying node 'network' and
#    changing params in:
#      sudo vim /etc/wpa_supplicant/wpa_supplicant.conf 
#    Tip: sudo iwlist wlan0 scan
#    Example (higer number is higher priority):
#      country=SI
#      
#      network={
#        ssid="<wifissid>"
#        psk="wifipasswd"
#        id_str="n9Wifi"
#        priority=2
#      }
#      network={
#        ssid="hotspotssid"
#        psk="hotspotpasswd"
#        id_str="motoWifi"
#        priority=1
#      }

sudo apt update
sudo apt upgrade
# gpiozero install 'pinout' command.
sudo apt install git vim silversearcher-ag omxplayer python3-gpiozero

# sudo raspi-config
# passwd
# sudo vi /etc/hostname 
# sudo vi /etc/hosts

# The following two lines do NOT remove python2 :-(
# sudo apt remove python2
# sudo apt remove python
cd /usr/bin/
sudo ln -fs python3 python
cd ~
git clone https://gitlab.com/markok314/alarmscale.git
cd alarmscale/resources
omxplayer submarine-diving-alarm-daniel_simon.mp3 

